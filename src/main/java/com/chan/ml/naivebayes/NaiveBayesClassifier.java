package com.chan.ml.naivebayes;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Notice - cl = Class Label, Yes = 1, No = 0
 */
public class NaiveBayesClassifier {
    private int columnCount;
    private ArrayList<HashMap<Double, Computable>> columns;
    private int clYesCount = 0, clNoCount = 0;
    private double clYesProbability = -1, clNoProbability = -1;

    public NaiveBayesClassifier(int _columnCount) {
        columnCount = _columnCount;
        columns = new ArrayList<>();

        for(int i = 1; i < columnCount; i++) {
            columns.add(new HashMap<>());
        }
    }

    private void calculateCLProbability() {
        int rowCount = clYesCount + clNoCount;
        clYesProbability = (double) clYesCount / rowCount;
        clNoProbability = (double) clNoCount / rowCount;
    }

    public double calculate(double[] row) {
        // row array must not include class label value
        if (row.length != columnCount - 1) return -1;
        if (clYesProbability == -1 || clNoProbability == -1)
            calculateCLProbability();

        double yesProbability = clYesProbability;
        double noProbability = clNoProbability;

        int i = 0;
        for(HashMap<Double, Computable> column: columns) {
            Computable computable = column.get(row[i++]);

            computable.setCLCounts(clYesCount, clNoCount);

            yesProbability *= computable.computeYesProbability();
            noProbability *= computable.computeNoProbability();
        }

        System.out.println("CLASS Label:Yes Probability " + yesProbability);
        System.out.println("CLASS Label:No Probability " + noProbability);

        return yesProbability > noProbability ? 1 : 0;
    }

    public void train(double[][] dataSet) {
        for(double[] row: dataSet) {
            addRow(row);
        }
    }

    public void addRow(double[] row) {
        // Class Label value must be at index 0 of row array
        if (row.length != columnCount) return;
        if (row[0] == 1.0) clYesCount++;
        else clNoCount++;

        for(int i = 1; i < columnCount; i++) {
            double key = row[i];
            HashMap<Double, Computable> counts = columns.get(i - 1);

            if (!counts.containsKey(key)) counts.put(key, new Computable());

            Computable computable = counts.get(key);
            computable.incrementCounts(row[0]);
        }
    }
}
