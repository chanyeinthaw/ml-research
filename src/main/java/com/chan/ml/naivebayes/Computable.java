package com.chan.ml.naivebayes;

class Computable {
    private int yesCount = 0, noCount = 0;
    private int clYesCount = 0, clNoCount = 0;

    void incrementCounts(double cl) {
        if (cl == 1) yesCount ++;
        else noCount ++;
    }

    void setCLCounts(int _clYesCount, int _clNoCount) {
        clYesCount = _clYesCount;
        clNoCount = _clNoCount;
    }

    double computeYesProbability() {
        return (double) yesCount / clYesCount;
    }

    double computeNoProbability() {
        return (double) noCount / clNoCount;
    }

    void log() {
        System.out.println("YC" + yesCount + "NC" + noCount);
        System.out.println("Y" + clYesCount + "N" + clNoCount);
    }
}
