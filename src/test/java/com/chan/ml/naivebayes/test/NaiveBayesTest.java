package com.chan.ml.naivebayes.test;

import com.chan.ml.naivebayes.NaiveBayesClassifier;

public class NaiveBayesTest {
    /**
     * NaiveBayesClassifier Info
     * Columns Buys Computer, Age, Income, Student, Credit Rating
     * Buys Computer (Yes = 1, No = 0) (Class Label)
     * Age (<= 30 = 1, 31..40 = 2, >40 = 3)
     * Income (High = 1, Medium = 2, Low = 3)
     * Student (Yes = 1, No = 0)
     * Credit Rating (Excellent = 1, Fair = 0)
     */
    public static void main(String[] args) {
        NaiveBayesClassifier ds = new NaiveBayesClassifier(5);

        ds.train(new double[][] {
            {0, 1, 1, 0, 0},
            {0, 1, 1, 0, 1},
            {1, 2, 1, 0, 0},
            {1, 3, 2, 0, 0},
            {1, 3, 3, 1, 0},
            {0, 3, 3, 1, 1},
            {1, 2, 3, 1, 1},
            {0, 1, 2, 0, 0},
            {1, 1, 3, 1, 0},
            {1, 3, 2, 1, 0},
            {1, 1, 2, 1, 1},
            {1, 2, 2, 0, 1},
            {1, 2, 1, 1, 0},
            {0, 3, 2, 0, 1}
        });

        System.out.println(ds.calculate(new double[]{1, 2, 1, 0}));
    }
}
